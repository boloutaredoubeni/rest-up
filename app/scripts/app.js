'use strict';

/**
 * @ngdoc overview
 * @name restUpApp
 * @description
 * # restUpApp
 *
 * Main module of the application.
 */
angular
  .module('restUpApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    // 'restUpApp.filters',
    // 'restUpApp.services',
    // 'restUpApp.directive',
    // 'restUpApp.controllers'
  ])
  // TODO: consider caching http requests
  .config(function ($routeProvider) {
    $routeProvider
       .when('/', {
         templateUrl: 'views/main.html',
         controller: 'MainCtrl',
         controllerAs: 'main'
       })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
