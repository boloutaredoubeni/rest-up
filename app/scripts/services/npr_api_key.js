'use strict';

/**
 * @ngdoc service
 * @name restUpApp.NPRAPIKEY
 * @description
 * # NPRAPIKEY
 * Constant in the restUpApp.
 */
angular.module('restUpApp')
  .constant('NPRAPIKEY', 'apiKey=');
