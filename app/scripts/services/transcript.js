'use strict';

/**
 * @ngdoc service
 * @name restUpApp.transcript
 * @description
 * # transcript
 * Factory in the restUpApp.
 */
angular.module('restUpApp')
  .factory('transcript', function ($http, storyID, NPRAPIKEY) {
    // Service logic
    // ...

    // NOTE: we do not need the story id's, we can pass the links provided by the api
    var _storyID = '';


    // Public API here
    return {
      setStoryID: function (storyID) {
        _storyID = storyID;
      },

      getStoryID: function() {
        return _storyID;
      },

      getTranscript: function() {
        var transcriptApiQuery = function() {
          // TODO: make sure these are all pased as strings
          // i.e. var storyId == id='[0-9]+'
          // NOTE: don't forget the ampersands
          var NPR_BASE_TRANSCRIPT_URL = 'http://api.npr.org/transcript?output=JSON&';
          var apiCall = NPR_BASE_TRANSCRIPT_URL + NPRAPIKEY;
          apiCall += '&id='+ _storyID;

          return apiCall;
        }();
        return $http
          .get(transcriptApiQuery)
          .then(function() {
            // on success
          }, function() {
            // on error
        });
      }
    };
  });
