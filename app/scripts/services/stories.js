'use strict';

/**
 * @ngdoc service
 * @name restUpApp.stories
 * @description
 * # stories
 * Factory in the restUpApp.
 */
angular.module('restUpApp')
  .factory('stories', function ($http, NPRAPIKEY) {
    // Service logic
    // ...
    // NOTE: npr api gives story text, transcript redundant
    var storiesAPIURL = 'http://api.npr.org/query?output=JSON&' + NPRAPIKEY;

    // Public API here
    return {
      getStories: function() {
        return $http.get(storiesAPIURL);
      }
    };
  });
