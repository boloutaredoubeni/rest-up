'use strict';

/**
 * @ngdoc function
 * @name restUpApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the restUpApp
 */
angular.module('restUpApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
