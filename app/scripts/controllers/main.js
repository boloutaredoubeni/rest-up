'use strict';

/**
 * @ngdoc function
 * @name restUpApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the restUpApp
 */
angular.module('restUpApp')
  .controller('MainCtrl', function ($scope, stories) {
    $scope.stories = {};

    $scope.storyList = [];

    // retrieve stories from the stories service
    stories
      .getStories()
      .then(function(retrievedStories) {
        $scope.stories = retrievedStories.data.list;
        $scope.storyList = retrievedStories.data.list.story;
      }, function(err) {
        $scope.stories = err;
      });

    $scope.makeDate = function(dateInput) {
      return new Date(dateInput);
    }
  });
