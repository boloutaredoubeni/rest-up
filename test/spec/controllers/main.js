'use strict';


describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('restUpApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  //describe('$scope.stories', function() {
  //
  //  it('Should have 5 attributes', function() {
  //    expect(_.size(scope.stories)).toBe(5);
  //  });
  //});
});
