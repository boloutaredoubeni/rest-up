'use strict';

describe('Service: NPRAPIKEY', function () {

  // load the service's module
  beforeEach(module('restUpApp'));

  // instantiate service
  var NPRAPIKEY;
  beforeEach(inject(function (_NPRAPIKEY_) {
    NPRAPIKEY = _NPRAPIKEY_;
  }));

  it('should do something', function () {
    expect(!!NPRAPIKEY).toBe(true);
  });

});
