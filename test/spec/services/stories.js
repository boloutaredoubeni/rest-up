'use strict';

describe('Service: stories', function () {

  // load the service's module
  beforeEach(module('restUpApp'));

  // instantiate service
  var stories, httpBackend, result;
  beforeEach(inject(function (_stories_, $httpBackend) {
    stories = _stories_;
    httpBackend = $httpBackend;

    httpBackend
      .whenGET('/.*/')
      .respond(readJSON('test/mock/stories.json'));

    stories
      .getStories()
      .then(function(res) {
        result = res;
      });

  }));

  //it('should do something', function () {
  //  expect(!!stories).toBe(true);
  //});
  it('should have 2 attributes', function() {

    expect(result.version).toBeDefined();
    expect(result.list).toBeDefined();
    expect(_.size(result)).toEqual(2);

  });

  it('should have a title', function() {
    expect(result.list.title).toBeDefined();
  });

  it('should have stories', function() {
    expect(_.isArray(result.list.story)).toBeTruthy();
    expect(result.list.story.length).toBeGreaterThan(0);
  });

});
