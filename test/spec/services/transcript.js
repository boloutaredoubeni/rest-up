'use strict';

describe('Service: transcript', function () {

  // load the service's module
  beforeEach(module('restUpApp'));

  // instantiate service
  var transcript;
  beforeEach(inject(function (_transcript_) {
    transcript = _transcript_;
  }));

  it('should do something', function () {
    expect(!!transcript).toBe(true);
  });

});
